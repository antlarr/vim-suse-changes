vim-suse-changes
################

Vim plugin for work with openSUSE \*.changes file.

``osurl`` script (which requires Lua and lua-lpeg) should be 
installed (or symlinked) to ``$PATH``

.. _`vim-snipmate`:
    https://github.com/honza/vim-snippets
